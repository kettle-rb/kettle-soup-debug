# frozen_string_literal: true

module Kettle
  module Soup
    module Debug
      VERSION = "0.1.0"
    end
  end
end
