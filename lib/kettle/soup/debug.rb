# frozen_string_literal: true

require_relative "debug/version"

module Kettle
  module Soup
    module Debug
      class Error < StandardError; end
      # Your code goes here...
    end
  end
end
